\documentclass{article}

\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{calc}
\usepackage{enumitem}
\usepackage[USenglish]{datetime2}

\title{Math 218D-2: Matrices and Vectors}
\author{Brian David Fitzpatrick}
\date{Summer 2024}
%
% \date{\DTMdisplaydate{2021}{08}{23}{-1}}

\begin{document}

\maketitle

\section*{Personnel}
This course consists of lectures facilitated by a lead instructor. Teaching
assistants will offer aid to students by holding weekly office hours.

\subsection*{Brian D.\ Fitzpatrick}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[Office] Physics 015
\item[E-Mail] \url{bfitzpat@math.duke.edu}
\item[URL] \url{https://bfitz.xyz}
\item[Office Hours] MTuTh 9:00 AM - 10:00 AM (Physics 015)
\end{description}

\subsection*{Ching-Lung Hsu}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{chinglung.hsu@duke.edu}
\item[Office Hours] MTh 7:00 PM - 10:00 PM (Gross Hall 355)\\
  F 1:00 PM - 2:00 PM (Zoom)\\
  F 2:00 PM - 5:00 PM (Gross Hall 358)
\end{description}

\subsection*{Victor Barbosa Slivinskis}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{victor.slivinskis@duke.edu}
\item[Office Hours] TuW 8:00 PM-9:00 PM (zoom)
\end{description}

\section*{Course Description}

Math 218D-2 is an introductory linear algebra course that focuses on concepts,
methods and applications. Gaussian elimination is presented as the fundamental
process for solving systems of linear equations. Deeper understanding is
developed by examination of matrix factorizations, orthogonality, and associated
vector subspaces. Least squares problems, projection problems, eigenvalue
problems, the singular value decomposition and principal component analysis are
studied as fundamental tools for solving data-driven problems.

The style of this course will be more applied and computational than Math 221
(for students focusing on becoming math majors), which goes into much more
depth on theory and develops skills in writing rigorous mathematical
proofs. Math 218 is also significantly different from Math 216 – differential
equations will be treated with less sophistication, but we will do more
advanced topics from linear algebra.

\section*{Schedule and Attendance}

Attendance is mandatory in each of the three weekly lectures.

The date, location, and time for each exam is as follows.

\begin{center}
  \begin{tabular}{*4l}\toprule
    Exam       & Date                              & Location       & Time               \\ \midrule
    Exam I     & \DTMdisplaydate{2024}{05}{30}{-1} & Physics 259    & (during lecture)   \\
    Exam II    & \DTMdisplaydate{2024}{06}{20}{-1} & Physics 259    & (during lecture)   \\
    \bottomrule
  \end{tabular}
\end{center}

For an excused absence during an exam, your final exam score will replace the
missed exam.

% \section*{Getting Help}

% In addition to attending office hours, students can get help by taking advantage
% of the math department's ``Help Room'' and the academic resource center's study
% sessions.
% \begin{description}[labelwidth=\widthof{\bfseries Study Sessions}, align=right, leftmargin=!]
% \item[Help Room] SuTh 7:00 PM - 10:00 PM (Physics 235)\\
%   \url{https://math.duke.edu/undergraduate/help-tutoring/help-rooms}
% \item[Study Sessions] Times TBA
%   % W 5:15 PM - 6:15 PM\\
%   % W 6:30 PM - 7:30 PM\\
%   % Th 5:00 PM - 6:00 PM\\
%   % F 9:00 AM - 10:00 AM\\
% \end{description}
% Visit the academic resource center's website (\url{arc.duke.edu}) or contact
% Suzanne Crifo (\url{suzanne.crifo@duke.edu}) to join a study session.


\section*{Resources}

The primary resource is the course webpage. Textbook problems will be
recommended for extra practice.
\begin{description}[labelwidth=\widthof{\bfseries Course Webpage}, align=right, leftmargin=!]
\item[Course Webpage] \url{https://bfitz.xyz/teaching/218su24t1}
\item[Textbook] Gilbert Strang, \emph{Introduction to Linear Algebra} (5th ed)\\
  ISBN-13: 978-0980232776
\end{description}

\section*{Grades}

Your work in this course will be weighted \emph{approximately} as follows.

\begin{center}
  \begin{tabular}{*2l}\toprule
    Item                  & Weight \\ \midrule
    Attendance          & 5\%   \\
    Comprehension Quizzes & 15\%   \\
    Midterm Exams          & 45\%   \\
    Final Exam            & 35\%   \\
    \bottomrule
  \end{tabular}
\end{center}

Each unexcused absence from lecture will result in a 2\% deduction from the raw
grade calculation.  This is only an approximation and I reserve the right to
determine grades as I see fit.

\section*{Problem Sets and Comprehension Quizzes}

Each Monday, problem sets and comprehension quizzes for each of the week's
topics will be released on gradescope. The deadline for submitting quizzes is
the following Tuesday at 10:30 PM. Late quizzes will not be accepted.

Problem sets will not be collected.

% It is your responsibility to print out a template of each problem set, write
% your solutions in the template, and upload your completed work to gradescope by
% the deadline. Comprehension quizzes are to be taken in gradescope.

Late work will not be accepted.

You are encouraged to collaborate with your peers on problem sets and
comprehension quizzes.

% \section*{Discussion Assignments}

% Discussion Assignments will be distributed at the beginning of each week's
% discussion session. These assignments consist of a double-sided single page
% worksheet. You are required to complete the assignment according to your
% discussion instructor's instructions. Discussion Assignments will be collected
% at the end of each discussion session and uploaded to gradescope, to be graded
% for \emph{completion only}.

\section*{Exams}

We will have two midterm exams and one final exam (dates and times listed
above). Each midterm exam is ``cumulative'' in the sense that you are expected
to have a proficient understanding of all material covered up to the cut-off
date for exam material. However, each midterm will primarily focus on material
covered since the previous exam. The final exam is a three-hour cumulative exam.

% \section*{Welcome Quiz Answer}

% Answer is: 52531

\section*{SDAO Accomodations}
If you receive SDAO accomodations for coursework, then I require that you inform
me of this by submitting a ``Professor Accommodation Letter'' to me by email. If
your accomodations apply to in-class exams, then you must set-up appointments to
take your exams at the university testing center. Information about the testing
center can be found at \url{https://testingcenter.duke.edu}.



\end{document}
