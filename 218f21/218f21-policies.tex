\documentclass{article}

\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{calc}
\usepackage{enumitem}
\usepackage[USenglish]{datetime2}

\title{Math 218D-2: Matrices and Vectors}
\author{Brian David Fitzpatrick}
\date{Fall 2021}
%
% \date{\DTMdisplaydate{2021}{08}{23}{-1}}

\begin{document}

\maketitle

\section*{Personnel}
This course has a lecture component lead by the lead instructor and discussion
components lead by discussion instructors.

\subsection*{Brian D.\ Fitzpatrick}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[Office] Physics 015
\item[E-Mail] \url{bfitzpat@math.duke.edu}
\item[URL] \url{https://bfitz.xyz}
\item[Office Hours] MWF 02:40 PM - 03:25 PM (White 107)\\
  Tu 04:30 PM - 05:15 PM (Zoom \texttt{987 3969 8047}, Password \texttt{linear})
\end{description}

\subsection*{Simon Hsu (Discussions 1 and 2)}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{chunhsien.hsu@duke.edu}
\item[Office Hours] Tu 10:00 AM - 12:00 PM (Physics 274J)
\end{description}

\subsection*{Curtis Porter (Discussions 3, 4, 5, and 6)}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{curtis.porter@duke.edu}
\item[Office Hours] Tu 01:00 PM - 03:00 PM (Physics 223)
\end{description}

\subsection*{Calvin McPhail-Snyder (Discussions 7 and 8)}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{calvin.mchphailsnyder@duke.edu}
\item[Office Hours] F 10:00 AM - 12:00 PM (Zoom \texttt{913 8198 9027}, Password \texttt{linear})
\end{description}

\subsection*{Logan Stokols (Discussions 9 and 10)}
\begin{description}[labelwidth=\widthof{\bfseries Office Hours}, align=right, leftmargin=!]
\item[E-Mail] \url{logan.stokols@duke.edu}
\item[Office Hours] W 11:00 AM - 01:00 PM (Physics 214)
\end{description}

\section*{Course Description}

Math 218D-2 is an introductory course on linear algebra that will focus on concepts,
methods and applications. Gaussian elimination is presented as the fundamental
process for solving systems of linear equations. Deeper understanding is
developed by examination of matrix factorizations, orthogonality, and
associated vector subspaces. Least squares problems, projection problems,
eigenvalue problems, the singular value decomposition and principal component
analysis will also be studied as fundamental tools for solving data-driven
problems.

The style of this course will be more applied and computational than Math 221
(for students focusing on becoming math majors), which goes into much more
depth on theory and develops skills in writing rigorous mathematical
proofs. Math 218 is also significantly different from Math 216 – differential
equations will be treated with less sophistication, but we will do more
advanced topics from linear algebra.

\section*{Schedule and Attendance}

Attendance is mandatory in each of the three weekly lectures.
\begin{description}
\item[Lecture 1] MWF 01:45 PM-02:35 PM (White 107)
\item[Lecture 2] MWF 03:30 PM-04:20 PM (White 107)
\end{description}
Attendance is also mandatory in each weekly discussion.
\begin{description}[labelwidth=\widthof{\bfseries Discussion 10}, align=right]
\item[Discussion 1] Th 08:30 AM-09:45 AM (Physics 235)
\item[Discussion 2] Th 10:15 AM-11:30 AM (Reuben-Cooke 129)
\item[Discussion 3] Th 12:00 PM-01:15 PM (Physics 154)
\item[Discussion 4] Th 01:45 PM-03:00 PM (Physics 154)
\item[Discussion 5] Th 03:30 PM-04:45 PM (Reuben-Cooke 127)
\item[Discussion 6] Th 05:15 PM-06:30 PM (Physics 259)
\item[Discussion 7] Th 10:15 AM-11:30 AM (Bio Sci 130)
\item[Discussion 8] Th 12:00 PM-01:15 PM (Allen 326)
\item[Discussion 9] Th 01:45 PM-03:00 PM (Allen 326)
\item[Discussion 10] Th 03:30 PM-04:45 PM (Social Sciences 119)
\end{description}
The date, location, and time for each exam is as follows.

\begin{center}
  \begin{tabular}{*4l}\toprule
    Exam       & Date                              & Location  & Time              \\ \midrule
    Exam I     & \DTMdisplaydate{2021}{09}{24}{-1} & White 107 & (during lecture)  \\
    Exam II    & \DTMdisplaydate{2021}{10}{22}{-1} & White 107 & (during lecture)  \\
    Exam III   & \DTMdisplaydate{2021}{11}{19}{-1} & White 107 & (during lecture)  \\
    Final Exam & \DTMdisplaydate{2021}{12}{10}{-1} & TBA       & 2:00 PM - 5:00 PM \\
    \bottomrule
  \end{tabular}
\end{center}

For an excused absence during an exam, your final exam score will replace the
missed exam.

\section*{Getting Help}

In addition to attending office hours, students can get help by taking advantage
of the math department's ``Help Room'' and the academic resource center's study
sessions.
\begin{description}[labelwidth=\widthof{\bfseries Study Sessions}, align=right, leftmargin=!]
\item[Help Room] MTh 7:00 PM - 10:00 PM (Physics 235)\\
  \url{https://math.duke.edu/undergraduate/help-tutoring/help-rooms}
\item[Study Sessions] W 5:15 PM - 6:15 PM\\
  W 6:30 PM - 7:30 PM\\
  Th 5:00 PM - 6:00 PM\\
  F 9:00 AM - 10:00 AM\\
\end{description}
Visit the academic resource center's website (\url{arc.duke.edu}) or contact
Suzanne Crifo (\url{suzanne.crifo@duke.edu}) to join a study session.


\section*{Resources}

The primary resource is the course webpage. Textbook problems will be
recommended for extra practice.
\begin{description}[labelwidth=\widthof{\bfseries Course Webpage}, align=right, leftmargin=!]
\item[Course Webpage] \url{https://bfitz.xyz/teaching/218f21}
\item[Textbook] Gilbert Strang, \emph{Introduction to Linear Algebra} (5th ed)\\
  ISBN-13: 978-0980232776
\end{description}


\section*{Grades}

Your work in this course will be weighted \emph{approximately} as follows.

\begin{center}
  \begin{tabular}{*2l}\toprule
    Item                  & Weight \\ \midrule
    Comprehension Quizzes & 8\%    \\
    Problem Sets          & 10\%   \\
    Discussion            & 12\%   \\
    Midterm Exams         & 40\%   \\
    Final Exam            & 30\%   \\
    \bottomrule
  \end{tabular}
\end{center}

This is only an approximation and I reserve the right to determine grades as I
see fit.

\section*{Problem Sets and Comprehension Quizzes}

Each Monday, problem sets and comprehension quizzes for each of the week's
topics will be released on gradescope. The deadline for submitting these problem
sets and quizzes is the following monday at 10:00 PM.

It is your responsibility to print out a template of each problem set, write
your solutions in the template, and upload your completed work to gradescope by
the deadline. Comprehension quizzes are to be taken in gradescope.

Late work will not be accepted.

You are encouraged to collaborate with your peers on problem sets and
comprehension quizzes.

\section*{Discussion Assignments}

Discussion Assignments will be distributed at the beginning of each week's
discussion session. These assignments consist of a double-sided single page
worksheet. You are required to complete the assignment according to your
discussion instructor's instructions. Discussion Assignments will be collected
at the end of each discussion session and uploaded to gradescope, to be graded
for \emph{completion only}.

\section*{Exams}

We will have three in-class midterm exams and one final exam (dates and times
listed above). Each midterm exam is ``cumulative'' in the sense that you are
expected to have a proficient understanding of all material covered up to the
cut-off date for exam material. However, each midterm will primarily focus on
material covered since the previous exam. The final exam is a three-hour
cumulative exam.

\section*{Welcome Quiz Answer}

Answer is: 3658

\section*{SDAO Accomodations}
If you receive SDAO accomodations for coursework, then I require that you inform
me of this by submitting a ``Professor Accommodation Letter'' to me by email. If
your accomodations apply to in-class exams, then you must set-up appointments to
take your exams at the university testing center. Information about the testing
center can be found at \url{https://testingcenter.duke.edu}.



\end{document}
