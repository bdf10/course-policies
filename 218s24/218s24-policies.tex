\documentclass{article}

\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{calc}
\usepackage{enumitem}
\usepackage[USenglish]{datetime2}

\title{Math 218D-2: Matrices and Vectors}
\author{Brian David Fitzpatrick}
\date{Spring 2024}
%
% \date{\DTMdisplaydate{2021}{08}{23}{-1}}

\begin{document}

\maketitle

\section*{Personnel}
This course has a lecture component lead by the lead instructor and discussion
components lead by discussion instructors.

\subsection*{Brian D.\ Fitzpatrick (Instructor)}
\begin{description}[labelwidth=\widthof{\bfseries E-Mail}, align=right, leftmargin=!]
\item[Office] Physics 015
\item[E-Mail] \url{bfitzpat@math.duke.edu}
\item[URL] \url{https://bfitz.xyz}
\end{description}

\subsection*{Jing An (Discussion Leader)}
\begin{description}[labelwidth=\widthof{\bfseries E-Mail}, align=right, leftmargin=!]
\item[E-Mail] \url{jing.an@duke.edu}
\end{description}

\subsection*{Blake Brown (TA)}
\begin{description}[labelwidth=\widthof{\bfseries E-Mail}, align=right, leftmargin=!]
\item[E-Mail] \url{blake.brown@duke.edu}
\end{description}

\subsection*{Ben Kelliher (TA)}
\begin{description}[labelwidth=\widthof{\bfseries E-Mail}, align=right, leftmargin=!]
\item[E-Mail] \url{benjamin.kelliher@duke.edu}
\end{description}

% \subsection*{Zach Chen (SAGE Group Leader)}
% \begin{description}[labelwidth=\widthof{\bfseries E-Mail}, align=right, leftmargin=!]
% \item[E-Mail] \url{zacharias.chen@duke.edu}
% \end{description}

\section*{Course Description}

Math 218D-2 is an introductory linear algebra course that focuses on concepts,
methods and applications. Gaussian elimination is presented as the fundamental
process for solving systems of linear equations. Deeper understanding is
developed by examination of matrix factorizations, orthogonality, and associated
vector subspaces. Least squares problems, projection problems, eigenvalue
problems, the singular value decomposition and principal component analysis are
studied as fundamental tools for solving data-driven problems.

The style of this course will be more applied and computational than Math 221
(for students focusing on becoming math majors), which goes into much more
depth on theory and develops skills in writing rigorous mathematical
proofs. Math 218 is also significantly different from Math 216 – differential
equations will be treated with less sophistication, but we will do more
advanced topics from linear algebra.

\section*{Schedule and Attendance}

Attendance is mandatory in each of the three weekly lectures and each weekly
discussion.

The date, location, and time for each exam is as follows.

\begin{center}
  \begin{tabular}{*4l}\toprule
    Exam       & Date                              & Location       & Time               \\ \midrule
    Exam I     & \DTMdisplaydate{2024}{02}{16}{-1} & White 107      & (during lecture)   \\
    Exam II    & \DTMdisplaydate{2024}{03}{22}{-1} & White 107      & (during lecture)   \\
    Exam III   & \DTMdisplaydate{2024}{04}{19}{-1} & White 107      & (during lecture)   \\
    Final Exam & \DTMdisplaydate{2024}{05}{03}{-1} & TBA            & 9:00 AM - 12:00 PM \\
    \bottomrule
  \end{tabular}
\end{center}

For an excused absence during an exam, your final exam score will replace the
missed exam.

\section*{Getting Help}

Weekly office hours will be held by the lead instructor, discussion leaders, and
teaching assistants. See the course calendar for specific times.

In addition to office hours, the academic resource center will hold study
sessions. Study session information will be announced through Sakai.


\section*{Resources}

The primary resource is the course webpage. Textbook problems will be
recommended for extra practice.
\begin{description}[labelwidth=\widthof{\bfseries Course Webpage}, align=right, leftmargin=!]
\item[Course Webpage] \url{https://bfitz.xyz/teaching/218s24}
\item[Textbook] Gilbert Strang, \emph{Introduction to Linear Algebra} (5th ed)\\
  ISBN-13: 978-0980232776
\end{description}

\section*{Grades}

Your work in this course will be weighted \emph{approximately} as follows.

\begin{center}
  \begin{tabular}{*2l}\toprule
    Item                  & Weight \\ \midrule
    Comprehension Quizzes & 8\%    \\
    Problem Sets          & 8\%   \\
    Discussion            & 10\%   \\
    Midterm Exams         & 42\%   \\
    Final Exam            & 32\%   \\
    \bottomrule
  \end{tabular}
\end{center}

This is only an approximation and I reserve the right to determine grades as I
see fit.

\section*{Problem Sets and Comprehension Quizzes}

Each Monday, problem sets and comprehension quizzes for each of the week's
topics will be released on gradescope. The deadline for submitting these problem
sets and quizzes is the following Tuesday at 11:30 PM.

It is your responsibility to print out a template of each problem set, write
your solutions in the template, and upload your completed work to gradescope by
the deadline. Comprehension quizzes are to be taken in gradescope.

Late work will not be accepted.

You are encouraged to collaborate with your peers on problem sets and
comprehension quizzes.

\section*{Discussion Assignments}

Discussion Assignments will be distributed at the beginning of each week's
discussion session. These assignments consist of a double-sided single page
worksheet. You are required to complete the assignment according to your
discussion instructor's instructions. Discussion Assignments will be collected
at the end of each discussion session and uploaded to gradescope, to be graded
for \emph{completion only}.

\section*{Exams}

We will have three in-class midterm exams and one final exam (dates and times
listed above). Each midterm exam is ``cumulative'' in the sense that you are
expected to have a proficient understanding of all material covered up to the
cut-off date for exam material. However, each midterm will primarily focus on
material covered since the previous exam. The final exam is a three-hour
cumulative exam.

\section*{Welcome Quiz Answer}

Answer is: 2479815

\section*{SDAO Accomodations}
If you receive SDAO accomodations for coursework, then I require that you inform
me of this by submitting a ``Professor Accommodation Letter'' to me by email. If
your accomodations apply to in-class exams, then you must set-up appointments to
take your exams at the university testing center. Information about the testing
center can be found at \url{https://testingcenter.duke.edu}.



\end{document}
